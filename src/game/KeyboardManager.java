package game;

import objects.characters.MainCharacter;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardManager implements KeyListener{

    private MainCharacter mainCharacter;

    public KeyboardManager(MainCharacter mainCharacter){
        this.mainCharacter = mainCharacter;
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (this.mainCharacter.isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.scene.getAxisXElementPosition() == -1) {
                    Main.scene.setAxisXElementPosition(0);
                    Main.scene.setBackgroundImageAxisXPosition(0,-50);
                    Main.scene.setBackgroundImageAxisXPosition(1, 750);
                }
                this.mainCharacter.setInMovement(true);
                this.mainCharacter.setMovingRight(true);
                Main.scene.setMovementIndex(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.scene.getAxisXElementPosition() == 4601) {
                    Main.scene.setAxisXElementPosition(4600);
                    Main.scene.setBackgroundImageAxisXPosition(0,-50);
                    Main.scene.setBackgroundImageAxisXPosition(1, 750);
                }

                this.mainCharacter.setInMovement(true);
                this.mainCharacter.setMovingRight(false);
                Main.scene.setMovementIndex(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                this.mainCharacter.setJumping(true);
                AudioManager.playSound("/res/sound/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.mainCharacter.setInMovement(false);
        Main.scene.setMovementIndex(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
