package game;

import objects.basics.*;
import objects.characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import objects.utilities.Pair;
import objects.utilities.Point2D;

@SuppressWarnings("serial")
public class GamePlatform extends JPanel {

    private ImageIcon backgroundIcon;
    private List<Pair<Image, Integer>> backgroundImages = new ArrayList<>(2);

    private ImageIcon startCastleIcon;
    private Image startCastleImage;
    private ImageIcon startIcon;
    private Image startImage;

    // bandiera e castello di fine
    private ImageIcon flagIcon;
    private Image flagImage;
    private ImageIcon endCastleIcon;
    private Image endCastleImage;

    //dimension con estremita a sinistra del background
    private int movementIndex = 0; // indice per il movimento che verra incrementato o decrementato
    private int axisXElementPosition;  // variabile per riperire gli elementi del gioco sul l'asse X
    private int groundHeight; // altezza del pavimento
    private int hauteurPlafond;

    private List<AnimatedCharacter> characters = new ArrayList<>(3);

    private Integer mainCharacterIndex = 0;

    private List<EnvironmentalObject> environmentalObjectsMap = new ArrayList<>(30);

    private List<Integer> moneysIndexMap; // table with all the piece

    //costrutore
    public GamePlatform() {

        super();

        this.axisXElementPosition = -1;
        this.groundHeight = 293;
        this.hauteurPlafond = 0;

        this.backgroundIcon = new ImageIcon(getClass().getResource("/res/images/background.png"));
        this.backgroundImages.add(new Pair<>(this.backgroundIcon.getImage(), -50));
        this.backgroundImages.add(new Pair<>(this.backgroundIcon.getImage(), 750));

        this.startCastleIcon = new ImageIcon(getClass().getResource("/res/images/castelloIni.png"));
        this.startCastleImage = this.startCastleIcon.getImage();

        this.startIcon = new ImageIcon(getClass().getResource("/res/images/startImage.png"));
        this.startImage = this.startIcon.getImage();

        this.endCastleIcon = new ImageIcon(getClass().getResource("/res/images/castelloF.png"));
        this.endCastleImage = endCastleIcon.getImage();

        this.flagIcon = new ImageIcon(getClass().getResource("/res/images/bandiera.png"));
        this.flagImage = flagIcon.getImage();

        this.characters.add(new Mario(new Point2D<>(300, 245)));
        this.characters.add(new Mushroom(new Point2D<>(800, 263)));
        this.characters.add(new Turtle(new Point2D<>(950, 243)));
        // position of all the object

        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(600, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(1000, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(1600, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(1900, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(2500, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(3000, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(3800, 230)));
        this.environmentalObjectsMap.add(new Pipe(new Point2D<>(4500, 230)));

        this.environmentalObjectsMap.add(new Block(new Point2D<>(400, 180)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(1200, 180)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(1270, 170)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(1340, 160)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(2000, 180)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(2600, 160)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(2650, 180)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(3500, 160)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(3550, 140)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(4000, 170)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(4200, 200)));
        this.environmentalObjectsMap.add(new Block(new Point2D<>(4300, 210)));

        this.environmentalObjectsMap.add(new Money(new Point2D<>(402, 145)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(1202, 140)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(1272, 95)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(1342, 40)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(1650, 145)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(2650, 145)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(3000, 135)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(3400, 125)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(4200, 145)));
        this.environmentalObjectsMap.add(new Money(new Point2D<>(4600, 40)));

        for (int index = 0; index < this.environmentalObjectsMap.size(); index ++){
            EnvironmentalObject environmentalObject = this.environmentalObjectsMap.get(index);
            if (environmentalObject instanceof Money){
                this.moneysIndexMap.add(index);
            }
        }

        //schermo listener
        super.setFocusable(true);
        super.requestFocusInWindow();

        //collegamento con la classe keyboard
        super.addKeyListener(new KeyboardManager((MainCharacter) this.characters.get(this.mainCharacterIndex)));
    }

    //getters
    public Integer getBackgroundImageAxisXPosition(final Integer backgroundIndex) {
        return this.backgroundImages.get(backgroundIndex).getSecond();
    }

    public int getGroundHeight() {
        return groundHeight;
    }

    public int getHauteurPlafond() {
        return hauteurPlafond;
    }

    public int getMovementIndex() {
        return this.movementIndex;
    }

    public int getAxisXElementPosition() {
        return axisXElementPosition;
    }

    //setters
    public void setBackgroundImageAxisXPosition(final Integer backgroundIndex, final Integer x) {
        this.backgroundImages.get(backgroundIndex).setSecond(x);
    }

    public void setGroundHeight(int groundHeight) {
        this.groundHeight = groundHeight;
    }

    public void setHauteurPlafond(int hauteurPlafond) {
        this.hauteurPlafond = hauteurPlafond;
    }

    public void setAxisXElementPosition(int axisXElementPosition) {
        this.axisXElementPosition = axisXElementPosition;
    }

    public void setMovementIndex(int movementIndex) {
        this.movementIndex = movementIndex;
    }

    // metodo per gestire la permanenza del fondo (si muove il background )
    public void lingeringMovementManager() {
        /* per bloccare la posizione il ritorno verso la sinistra con il castello poi si aggiorna il valore
		 di xpos dentro la classe keyboard affinche non sia mai negativo */
        if (this.axisXElementPosition >= 0 && this.axisXElementPosition <= 4600) {
            this.axisXElementPosition = this.axisXElementPosition + this.getMovementIndex();

            for (int index = 0; index < this.backgroundImages.size(); index ++){
                this.setBackgroundImageAxisXPosition(index, this.getBackgroundImageAxisXPosition(index) - this.movementIndex);
            }
        }

        //condizioni per aggiornamento del background a l'infinito
        if (this.getBackgroundImageAxisXPosition(0) == -800) {
            // dove il background 1 finisce si mette il 2 (a destra )
            this.setBackgroundImageAxisXPosition(0, 800);

        } else if (this.getBackgroundImageAxisXPosition(1) == -800) {
            // dove il background 2 finisce si mette il 1 (a destra )
            this.setBackgroundImageAxisXPosition(1, 800);

        } else if (this.getBackgroundImageAxisXPosition(0) == 800) {
            // dove il background 1 finisce si mette il 2 (a sinistra )
            this.setBackgroundImageAxisXPosition(0, -800);

        } else if (this.getBackgroundImageAxisXPosition(1) == 800) {
            // dove il background 2 finisce si mette il 1 (a sinistra )
            this.setBackgroundImageAxisXPosition(1, -800);
        }
    }

    // disegno dei component
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;
        //detezione collision con l'oggetto piu isNear di lui

        for (int index = 0; index < environmentalObjectsMap.size(); index++) {
            for (AnimatedCharacter ac : characters){
                if(ac.isNear(environmentalObjectsMap.get(index))){
                    ac.collision(environmentalObjectsMap.get(index));
                }
            }
        }

        //detection with the piece

        MainCharacter mainCharacter = (Mario) this.characters.get(this.mainCharacterIndex);

        for (int index = 0; index < moneysIndexMap.size(); index++) {
            Money money = (Money) this.environmentalObjectsMap.get(this.moneysIndexMap.get(index));
            if (mainCharacter.isCollidingWithAPiece(money)) {
                AudioManager.playSound("/res/sound/money.wav");
                this.moneysIndexMap.remove(index);
            }
        }

        for (AnimatedCharacter characterA : this.characters){
            for (AnimatedCharacter characterB : this.characters){
                if (characterA != characterB && characterA.isNear(characterB)){
                    characterA.collision(characterB);
                }
            }
        }

        // spostamento oggetti fissi
        this.lingeringMovementManager();

        if (this.axisXElementPosition >= 0 && this.axisXElementPosition <= 4600) {

            for (EnvironmentalObject environmentalObject : this.environmentalObjectsMap) {
                environmentalObject.move();
            }
            for (AnimatedCharacter character : this.characters){
                if (!(character instanceof MainCharacterImpl)){
                    character.move();
                }
            }
        }
        for (Pair<Image, Integer> bg : this.backgroundImages){
            g2.drawImage(bg.getFirst(), bg.getSecond(), 0, null);
        }

        g2.drawImage(this.startCastleImage, 10 - this.axisXElementPosition, 95, null);
        g2.drawImage(this.startImage, 220 - this.axisXElementPosition, 234, null);

        // disegno di tutti gli oggetti
        for (EnvironmentalObject environmentalObject : this.environmentalObjectsMap) {
            g2.drawImage(environmentalObject.getImageObject(), environmentalObject.getPosition().getX(),
                    environmentalObject.getPosition().getY(), null);
        }

        // disegno castello fine e bandiera
        g2.drawImage(this.flagImage, 4650 - this.axisXElementPosition, 115, null);
        g2.drawImage(this.endCastleImage, 4850 - this.axisXElementPosition, 145, null);

        //disegno di mario

        for (AnimatedCharacter character : this.characters){


            if (character.isAlive()) {
                if (character.isJumping()){
                    g2.drawImage(character.jumpAnimation().getImage(), character.getPosition().getX(),
                            character.getPosition().getY(), null);
                } else {
                    g2.drawImage(character.baseAnimation(35).getImage(), character.getPosition().getX(),
                            character.getPosition().getY(), null);
                }

            } else {
                character.setAlive(false);
                g2.drawImage(character.deathAnimation().getImage(), character.getPosition().getX(),
                        character.getPosition().getY() + 25, null);
            }
        }
    }

}
