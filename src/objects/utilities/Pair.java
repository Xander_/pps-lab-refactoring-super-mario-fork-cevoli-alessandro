package objects.utilities;

/**
 *
 * Created by Xander_C on 28/02/2017.
 */
public class Pair<X, Y> {

    private X first;
    private Y second;

    public  Pair(final X first, final Y second ){
        this.first = first;
        this.second = second;
    }

    public X getFirst() {
        return first;
    }

    public Y getSecond() {
        return second;
    }

    public void setFirst(X first) {
        this.first = first;
    }

    public void setSecond(Y second) {
        this.second = second;
    }
}
