package objects.utilities;

/**
 *
 * Created by Xander_C on 28/02/2017.
 */
public class Dimension2D<X, Y> extends Pair<X, Y> {

    public Dimension2D(final X length, final Y height){
        super(length, height);
    }

    public X getLength(){
        return this.getFirst();
    }

    public Y getHeight(){
        return this.getSecond();
    }

    public void setLength(X length) {
        this.setFirst(length);
    }

    public void setHeight(Y height) {
        this.setSecond(height);
    }
}
