package objects.characters;

import objects.basics.GameObject;
import objects.basics.AnimatedObject;

import javax.swing.*;

/**
 *
 * Created by Xander_C on 03/03/2017.
 */
public interface AnimatedCharacter extends AnimatedObject {

    ImageIcon jumpAnimation();

    ImageIcon deathAnimation();

    boolean isMoving();

    boolean isMovingRight();

    boolean isJumping();

    boolean isAlive();

    void setInMovement(final boolean condition);

    void setMovingRight(final boolean condition);

    void setJumping(final boolean condition);

    void setAlive(final boolean condition);

    boolean isNear(final GameObject object);

    void collision(final GameObject object);

}
