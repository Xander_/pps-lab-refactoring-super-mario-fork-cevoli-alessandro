package objects.characters;

import objects.basics.GameObject;
import objects.utilities.Dimension2D;
import objects.basics.EnvironmentalObjectImpl;
import objects.utilities.Point2D;

import javax.swing.*;

/**
 *
 * Created by Xander_C on 02/03/2017.
 */
public abstract class EnemyCharacter extends AnimatedCharacterImpl implements Runnable{

    private final int PAUSE = 15;
    private int directionOfMovement = 1;

    EnemyCharacter(Point2D<Integer> position, Dimension2D<Integer, Integer> dimension) {
        super(position, dimension);
    }

    @Override
    public boolean isJumping() {
        return false;
    }

    @Override
    public void move(){

        if(super.isMovingRight()){
            this.changeDirectionOfMovement(1);
        } else {
            this.changeDirectionOfMovement(-1);
        }

        super.getPosition().setX(super.getPosition().getX() + this.getDirectionOfMovement());

    }

    private Integer getDirectionOfMovement(){

        return this.directionOfMovement;
    }

    private void changeDirectionOfMovement(final Integer direction){

        this.directionOfMovement = direction;
    }

    @Override
    public void run() {

        try{
            Thread.sleep(20);
        } catch(InterruptedException e){
            // Do Nothing
        }

        while(true){
            if(this.isAlive()){
                this.move();
                try{
                    Thread.sleep(PAUSE);
                } catch(InterruptedException e){
                    // Do Nothing
                }
            }
        }
    }

    @Override
    public void collision(GameObject obj){
        if(super.isCollidingForward(obj) && this.isMovingRight()){

            super.setMovingRight(false);
            this.changeDirectionOfMovement(-1);

        } else if (super.isCollidingBackward(obj) && this.isMovingRight()){

            super.setMovingRight(true);
            this.changeDirectionOfMovement(1);
        }
    }
}
