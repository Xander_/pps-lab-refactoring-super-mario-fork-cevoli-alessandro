package objects.characters;

import javax.swing.ImageIcon;

import game.Main;
import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

public class Mario extends MainCharacterImpl {

    private static final String CHARACTER_NAME = "mario";
    private static final String KILLED_RIGHT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "ED.png";
    private static final String KILLED_LEFT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "EG.png";
    private static final String ALIVE_STANDING_RIGHT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "AD.png";
    private static final String ALIVE_STANDING_LEFT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "AG.png";
    private static final String ALIVE_MOVING_RIGHT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "AD.png";
    private static final String ALIVE_MOVING_LEFT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "AG.png";
    private static final String JUMPING_RIGHT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "SD.png";
    private static final String JUMPING_LEFT_MARIO_IMAGE = "/res/images/" + CHARACTER_NAME + "SG.png";
	
	public Mario(final Point2D<Integer> position) {
		super(position, new Dimension2D<>(28, 50));
		super.setIconObject(new ImageIcon(getClass().getResource("/res/images/marioD.png")));
        super.setImageObject(this.getIconObject().getImage());
	}

    @Override
    public boolean moveCondition(){

        return !this.isMoving() ||
                Main.scene.getAxisXElementPosition() <= 0 ||
                Main.scene.getAxisXElementPosition() > 4600;
    }

    @Override
    public ImageIcon deathAnimation(){
        return this.baseAnimation(35);
    }

    @Override
    protected String getDeathImagePath(boolean condition) {
        return condition ? KILLED_RIGHT_MARIO_IMAGE :
                KILLED_LEFT_MARIO_IMAGE;
    }

    @Override
    protected String getStandingImagePath(boolean condition) {
        return condition ? ALIVE_STANDING_RIGHT_MARIO_IMAGE :
                ALIVE_STANDING_LEFT_MARIO_IMAGE;
    }

    @Override
    protected String getMovingImagePath(boolean condition) {
        return condition ? ALIVE_MOVING_RIGHT_MARIO_IMAGE :
                ALIVE_MOVING_LEFT_MARIO_IMAGE;
    }

    @Override
    protected String getJumpingImagePath(boolean condition) {
        return condition ? JUMPING_RIGHT_MARIO_IMAGE :
                JUMPING_LEFT_MARIO_IMAGE;
    }
}