package objects.characters;

import game.Main;
import objects.basics.*;
import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

/**
 *
 * Created by Xander_C on 02/03/2017.
 */
public abstract class MainCharacterImpl extends AnimatedCharacterImpl implements MainCharacter{


    MainCharacterImpl(Point2D<Integer> position, Dimension2D<Integer, Integer> dimension) {
        super(position, dimension);
    }

    public void collision(GameObject object){

        if(object instanceof AnimatedCharacter) {
            this.collisionWithACharacter((AnimatedCharacter) object);
        } if(object instanceof EnvironmentalObject){
            this.collisionWithTheEnvironment((EnvironmentalObject) object);
        }
    }

    public boolean isCollidingWithAPiece(Money money){

        return this.isCollidingBackward(money) ||
                this.isCollidingTop(money) ||
                this.isCollidingForward(money)
                || this.isCollidingBottom(money);
    }

    private void collisionWithTheEnvironment(EnvironmentalObject object){

        if(this.isCollidingForward(object) && this.isMovingRight() ||
                (this.isCollidingBackward(object)) && this.isMovingRight()){
            Main.scene.setMovementIndex(0);
            this.setInMovement(false);
        }

        if(this.isCollidingBottom(object) && this.isJumping()){ // mario salta su un oggetto
            Main.scene.setGroundHeight(object.getPosition().getY());
        } else if (this.isCollidingBottom(object)) { // mario cade sul pavimento
            Main.scene.setGroundHeight(293);  // 293 che � il valore iniziale

            if (this.isJumping()) {
                this.getPosition().setY(243); // altezza iniziale di mario
            }

            // collision con un oggetto sopra
            if (this.isCollidingTop(object)) {
                Main.scene.setHauteurPlafond(object.getPosition().getY()
                        + object.getDimension().getHeight()); // il nuovo cielo diventa il sotto del oggetto
            } else if (this.isCollidingTop(object) && this.isJumping()) {
                Main.scene.setHauteurPlafond(0); // cielo iniziale
            }
        }
    }

    private void collisionWithACharacter(AnimatedCharacter character){

        if((this.isCollidingForward(character) || this.isCollidingBackward(character))
                && character.isAlive()){
            this.setInMovement(false);
            this.setAlive(false);
        } else if(super.isCollidingBottom(character)){
            this.setAlive(true);
            character.setInMovement(false);
            character.setAlive(false);
        }
    }
}
