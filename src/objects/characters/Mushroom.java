package objects.characters;

import javax.swing.ImageIcon;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

public class Mushroom extends EnemyCharacter{

    public static final String CHARACTER_NAME = "mushroom";
    private static final String KILLED_RIGHT_MUSHROOM_IMAGE = "/res/images/" + CHARACTER_NAME + "ED.png";
    private static final String KILLED_LEFT_MUSHROOM_IMAGE = "/res/images/" + CHARACTER_NAME + "EG.png";
    private static final String ALIVE_STANDING_RIGHT_MUSHROOM_IMAGE = "/res/images/" + CHARACTER_NAME + "AD.png";
    private static final String ALIVE_STANDING_LEFT_MUSHROOM_IMAGE = "/res/images/" + CHARACTER_NAME + "AG.png";
    private static final String ALIVE_MOVING_RIGHT_MUSHROOM_IMAGE = "/res/images/" + CHARACTER_NAME + "AD.png";
    private static final String ALIVE_MOVING_LEFT_MUSHROOM_IMAGE = "/res/images/" + CHARACTER_NAME + "AG.png";

	
	public Mushroom(final Point2D<Integer> position) {

		super(position, new Dimension2D<>(27, 30));
		super.setMovingRight(true);
		super.setInMovement(true);
		super.setIconObject(new ImageIcon(getClass().getResource(ALIVE_STANDING_RIGHT_MUSHROOM_IMAGE)));
        super.setImageObject(this.getIconObject().getImage());
		
		Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
	}

    @Override
    protected String getDeathImagePath(boolean condition) {
        return condition ? Mushroom.KILLED_RIGHT_MUSHROOM_IMAGE :
                Mushroom.KILLED_LEFT_MUSHROOM_IMAGE;
    }

    @Override
    protected String getStandingImagePath(boolean condition) {
        return condition ? Mushroom.ALIVE_STANDING_RIGHT_MUSHROOM_IMAGE :
                Mushroom.ALIVE_STANDING_LEFT_MUSHROOM_IMAGE;
    }

    @Override
    protected String getMovingImagePath(boolean condition) {
        return condition ? ALIVE_MOVING_RIGHT_MUSHROOM_IMAGE :
                Mushroom.ALIVE_MOVING_LEFT_MUSHROOM_IMAGE;
    }

    @Override
    protected String getJumpingImagePath(boolean condition) {
        return null;
    }

    @Override
    public boolean moveCondition() {
        return !this.isMoving();
    }
}
