package objects.characters;

import javax.swing.*;

import game.Main;
import objects.basics.AnimatedObjectImpl;
import objects.basics.EnvironmentalObjectImpl;
import objects.basics.GameObject;
import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

public abstract class AnimatedCharacterImpl extends AnimatedObjectImpl implements AnimatedCharacter {

    private static final Integer PROXIMITY_OFFSET = 10;

    private boolean isMoving = false; // vero quando il personnagio camina
    private boolean isMovingRight = true; // vero quando camina verso la destro false verso la sinistra
    private boolean isAlive = true;
    private boolean isJumping = false;
    private Integer count = 0;
    private Integer jumpIntensity = 0;

    AnimatedCharacterImpl(Point2D<Integer> position, Dimension2D<Integer, Integer> dimension) {

        super(position, dimension);
    }

    protected abstract String getMovingImagePath(final boolean condition);

    protected abstract String getJumpingImagePath(final boolean condition);

    protected abstract String getDeathImagePath(final boolean condition);

    @Override
    public boolean isMoving() {
        return this.isAlive() && this.isMoving;
    }

    @Override
    public void setInMovement(final boolean condition) {
        this.isMoving = condition;
    }

    @Override
    public boolean isMovingRight() {
        return this.isAlive() && isMovingRight;
    }

    @Override
    public void setMovingRight(final boolean condition) {
        this.isMovingRight = condition;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public void setAlive(final boolean condition) {
        this.isAlive = condition;
    }

    @Override
    public boolean isJumping() {
        return this.isAlive() && this.isJumping;
    }

    @Override
    public void setJumping(final boolean condition) {
        this.isJumping = condition;
    }

    @Override
    public ImageIcon baseAnimation(final Integer frequency) {

        String str;

        if (this.moveCondition()) {
            str = this.getStandingImagePath(this.isMovingRight());
        } else {

            this.count++;

            str = this.count / frequency == 0 ?
                    this.getStandingImagePath(this.isMovingRight()) :
                    this.getMovingImagePath(this.isMovingRight());

            if (this.count == 2 * frequency) {
                this.count = 0;
            }
        }

        return new ImageIcon(getClass().getResource(str));
    }

    @Override
    public ImageIcon jumpAnimation() {

        this.jumpIntensity++;

        if (this.jumpIntensity <= 49) {
            if (this.getPosition().getY() > Main.scene.getHauteurPlafond()) {
                this.getPosition().setY(this.getPosition().getY() - 5);
            } else {
                this.jumpIntensity = 50;
            }

        } else if (this.getPosition().getY() + this.getDimension().getHeight() < Main.scene.getGroundHeight()) {

            this.getPosition().setY(this.getPosition().getY() + 1);

        } else {

            this.setJumping(false);
            this.jumpIntensity = 0;
        }

        return new ImageIcon(getClass().getResource(this.getJumpingImagePath(this.isMovingRight())));

    }

    @Override
    public ImageIcon deathAnimation() {
        return new ImageIcon(getClass().getResource(
                this.getDeathImagePath(this.isMovingRight())));
    }

    public boolean isNear(final GameObject object) {

        return (this.getPosition().getX() > object.getPosition().getX() - PROXIMITY_OFFSET &&
                this.getPosition().getX() < object.getPosition().getX() + object.getDimension().getLength() + PROXIMITY_OFFSET) ||
                (this.getPosition().getX() + this.getDimension().getLength() > object.getPosition().getX() - PROXIMITY_OFFSET &&
                        this.getPosition().getX() + this.getDimension().getLength() < object.getPosition().getX() + object.getDimension().getLength() + PROXIMITY_OFFSET);
    }

    @Override
    public abstract void collision(final GameObject object);

    // detezione collision a destra
    boolean isCollidingForward(final GameObject object) {

        return ((object instanceof AnimatedCharacterImpl && this.isMovingRight()) || object instanceof EnvironmentalObjectImpl) &&
                !(this.getPosition().getX() + this.getDimension().getLength() < object.getPosition().getX() ||
                        this.getPosition().getX() + this.getDimension().getLength() > object.getPosition().getX() + 5 ||
                        this.getPosition().getY() + this.getDimension().getHeight() <= object.getPosition().getY() ||
                        this.getPosition().getY() >= object.getPosition().getY() + object.getDimension().getHeight());

    }

    // detezione collision a sinistra
    boolean isCollidingBackward(final GameObject object) {

        return !(this.getPosition().getX() > object.getPosition().getX() + object.getDimension().getLength() ||
                this.getPosition().getX() + this.getDimension().getLength() < object.getPosition().getX() +
                        object.getDimension().getLength() - 5 ||
                this.getPosition().getY() + this.getDimension().getHeight() <= object.getPosition().getY() ||
                this.getPosition().getY() >= object.getPosition().getY() + object.getDimension().getHeight());
    }

    //collision sotto
    boolean isCollidingBottom(final GameObject object) {

        return !(this.getPosition().getX() + this.getDimension().getLength() < object.getPosition().getX() + 5 ||
                this.getPosition().getX() > object.getPosition().getX() + object.getDimension().getLength() - 5 ||
                this.getPosition().getY() + this.getDimension().getHeight() < object.getPosition().getY() ||
                this.getPosition().getY() + this.getDimension().getHeight() > object.getPosition().getY() + 5);
    }

    // collision in alto
    boolean isCollidingTop(final GameObject object) {
        return !(this.getPosition().getX() + this.getDimension().getLength() < object.getPosition().getX() + 5 ||
                this.getPosition().getX() > object.getPosition().getX() + object.getDimension().getLength() - 5 ||
                this.getPosition().getY() < object.getPosition().getY() + object.getDimension().getHeight() ||
                this.getPosition().getY() > object.getPosition().getY() + object.getDimension().getHeight() + 5);
    }
}
