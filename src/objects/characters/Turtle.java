package objects.characters;

import javax.swing.ImageIcon;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

public class Turtle extends EnemyCharacter implements Runnable {

    public static final String CHARACTER_NAME = "turtle";

    private static final String KILLED_RIGHT_TURTLE_IMAGE = "/res/images/" + CHARACTER_NAME + "ED.png";
    private static final String KILLED_LEFT_TURTLE_IMAGE = "/res/images/" + CHARACTER_NAME + "EG.png";
    private static final String ALIVE_STANDING_RIGHT_TURTLE_IMAGE = "/res/images/" + CHARACTER_NAME + "AD.png";
    private static final String ALIVE_STANDING_LEFT_TURTLE_IMAGE = "/res/images/" + CHARACTER_NAME + "AG.png";
    private static final String ALIVE_MOVING_RIGHT_TURTLE_IMAGE = "/res/images/" + CHARACTER_NAME + "AD.png";
    private static final String ALIVE_MOVING_LEFT_TURTLE_IMAGE = "/res/images/" + CHARACTER_NAME + "AG.png";

	
	public Turtle(final Point2D<Integer> position) {

        super(position, new Dimension2D<>(43, 50));
        super.setMovingRight(true);
        super.setInMovement(true);
        super.setIconObject(new ImageIcon(getClass().getResource("/res/images/turtleAD.png")));
        super.setImageObject(this.getIconObject().getImage());
		
		Thread chronoTurtle = new Thread(this);
		chronoTurtle.start();
	}

    @Override
    protected String getDeathImagePath(boolean condition) {
        return condition ? KILLED_RIGHT_TURTLE_IMAGE : KILLED_LEFT_TURTLE_IMAGE;
    }

    @Override
    protected String getStandingImagePath(boolean condition) {
        return condition ? ALIVE_STANDING_RIGHT_TURTLE_IMAGE : ALIVE_STANDING_LEFT_TURTLE_IMAGE;
    }

    @Override
    protected String getMovingImagePath(boolean condition) {
        return condition ? ALIVE_MOVING_RIGHT_TURTLE_IMAGE : ALIVE_MOVING_LEFT_TURTLE_IMAGE;
    }

    @Override
    protected String getJumpingImagePath(boolean condition) {
        return null;
    }

    @Override
    public boolean moveCondition() {
        return !this.isMoving();
    }
}
