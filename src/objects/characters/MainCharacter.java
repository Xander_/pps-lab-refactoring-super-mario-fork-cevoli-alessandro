package objects.characters;

import objects.basics.Money;

/**
 *
 * Created by Xander_C on 05/03/2017.
 */
public interface MainCharacter extends AnimatedCharacter{

    boolean isCollidingWithAPiece(Money money);
}
