package objects.basics;

import javax.swing.*;
import java.awt.*;

/**
 *
 * Created by Xander_C on 05/03/2017.
 */
public interface EnvironmentalObject extends GameObject {

    Image getImageObject();

    ImageIcon getIconObject();

    void setIconObject(ImageIcon iconObject);

    void setImageObject(Image imageObject);

    void move();
}
