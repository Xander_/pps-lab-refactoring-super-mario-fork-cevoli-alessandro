package objects.basics;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

import javax.swing.*;

/**
 *
 * Created by Xander_C on 03/03/2017.
 */
public abstract class AnimatedObjectImpl extends EnvironmentalObjectImpl implements AnimatedObject {

    public AnimatedObjectImpl(Point2D<Integer> position, Dimension2D<Integer, Integer> dimension) {
        super(position, dimension);
    }

    protected abstract String getStandingImagePath(final boolean condition);

    public abstract boolean moveCondition();

    @Override
    public abstract ImageIcon baseAnimation(final Integer frequency);
}
