package objects.basics;

import java.awt.Image;

import javax.swing.ImageIcon;

import game.Main;
import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

public abstract class EnvironmentalObjectImpl extends GameObjectImpl implements EnvironmentalObject {

	private Image imageObject;
	private ImageIcon iconObject;

	EnvironmentalObjectImpl(final Point2D<Integer> position,
                            final Dimension2D<Integer, Integer> dimension){

	    super(position, dimension);
	}

	@Override
	public Image getImageObject() {
		return imageObject;
	}

    @Override
	public ImageIcon getIconObject() {
		return iconObject;
	}

    @Override
	public void setIconObject(ImageIcon iconObject) {

		this.iconObject = iconObject;
    }

    @Override
    public void setImageObject(Image imageObject) {

	    this.imageObject = imageObject;
    }

    @Override
	public void move(){

		if(Main.scene.getAxisXElementPosition() >= 0){
			this.getPosition().setX(this.getPosition().getX() - Main.scene.getMovementIndex());
		}
	}

}
