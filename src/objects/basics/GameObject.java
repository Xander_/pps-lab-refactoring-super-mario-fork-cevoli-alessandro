package objects.basics;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

/**
 *
 * Created by Xander_C on 05/03/2017.
 */
public interface GameObject {

    Dimension2D<Integer, Integer> getDimension();

    Point2D<Integer> getPosition();

    void setPosition(final Point2D<Integer> newPosition);

    void setDimension(final Dimension2D<Integer, Integer> newDimension);

}
