package objects.basics;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

/**
 *
 * Created by Xander_C on 28/02/2017.
 */
public abstract class GameObjectImpl implements GameObject{
    private Dimension2D<Integer, Integer> dimension;
    private Point2D<Integer> position;

    GameObjectImpl(Point2D<Integer> position, Dimension2D<Integer, Integer> dimension){
        this.setPosition(position);
        this.setDimension(dimension);
    }

    @Override
    public Dimension2D<Integer, Integer> getDimension() {
        return this.dimension;
    }

    @Override
    public Point2D<Integer> getPosition() {
        return this.position;
    }

    @Override
    public void setPosition(final Point2D<Integer> newPosition) {
        this.position = newPosition;
    }

    @Override
    public void setDimension(Dimension2D<Integer, Integer> newDimension) {
        this.dimension = newDimension;
    }
}
