 package objects.basics;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

import javax.swing.ImageIcon;

public class Pipe extends EnvironmentalObjectImpl {

	public Pipe(final Point2D<Integer> position) {
		
		super(position, new Dimension2D<>(43, 65));
        super.setIconObject(new ImageIcon(getClass().getResource("/res/images/Blocco.png")));
        super.setImageObject(this.getIconObject().getImage());
	}

}
