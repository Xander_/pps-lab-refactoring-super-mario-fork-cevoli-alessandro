package objects.basics;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

import javax.swing.ImageIcon;

public class Block extends EnvironmentalObjectImpl {
	
	public Block(final Point2D<Integer> position) {
		
		super(position, new Dimension2D<>(30, 30));
		super.setIconObject(new ImageIcon(getClass().getResource("/res/images/Blocco.png")));
        super.setImageObject(this.getIconObject().getImage());
		//super.setImageObject(super.getIconObject().getImage());
	}

}
