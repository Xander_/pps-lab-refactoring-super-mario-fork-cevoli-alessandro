package objects.basics;

import objects.utilities.Dimension2D;
import objects.utilities.Point2D;

import javax.swing.ImageIcon;

public class Money extends AnimatedObjectImpl implements Runnable{

    private static final String CHARACTER_NAME = "money";
    private static final String VISIBLE_MONEY_IMAGE = "/res/images/" + CHARACTER_NAME + "1.png";
    private static final String INVISIBLE_MONEY_IMAGE = "/res/images/" + CHARACTER_NAME + ".png";

	private int count;
	private final int PAUSE = 10;

	public Money(final Point2D<Integer> position) {
        super(position, new Dimension2D<Integer, Integer>(30 ,30));
		super.setIconObject(new ImageIcon(this.getClass().getResource(VISIBLE_MONEY_IMAGE)));
        super.setImageObject(this.getIconObject().getImage());
	}

	@Override
	public ImageIcon baseAnimation(final Integer frequency){

		this.count++;

        String str = this.moveCondition() ? VISIBLE_MONEY_IMAGE : INVISIBLE_MONEY_IMAGE;

		if (this.count == 2 * frequency ) {
		    this.count = 0;
		}

		return new ImageIcon(getClass().getResource(str));
	}

    @Override
	public void run() {
		
		try{
		    Thread.sleep(10);
		} catch(InterruptedException e){
		    // Do Nothing ?
        }
			
		while(true){

			this.setIconObject(this.baseAnimation(100));
			this.setImageObject(this.getIconObject().getImage());

			try{
                Thread.sleep(PAUSE);
			}catch(InterruptedException e){
			    // Do Nothing ?
            }
		}
		
	}

    @Override
    protected String getStandingImagePath(boolean condition) {
        return this.moveCondition() ? VISIBLE_MONEY_IMAGE : INVISIBLE_MONEY_IMAGE;
    }

    @Override
    public boolean moveCondition() {
        return this.count / 100 == 0;
    }
}
