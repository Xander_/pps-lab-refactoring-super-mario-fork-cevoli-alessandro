package objects.basics;

import javax.swing.*;

/**
 *
 * Created by Xander_C on 03/03/2017.
 */
public interface AnimatedObject extends EnvironmentalObject{

    ImageIcon baseAnimation(final Integer frequency);
}
